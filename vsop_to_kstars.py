import sys
import re

infile = sys.argv[1];

data = []
header = {}

def printFile():
    outputData = [' '.join(line) for line in data]
    outputData = ''.join(outputData)

    with open('./'+header['planet']+'.'+header['variable']+header['coefficient']+'.vsop', 'w+') as of:
        of.write(outputData);

lineSplit = re.compile(' +')
with open(infile) as f:
    for line in f:
        splitLine = lineSplit.split(line);
        splitLine = [field for field in splitLine if field != '']

        if splitLine[0][0] == 'V':
            if not len(data) == 0:
                printFile()

            header = {
                'planet': splitLine[3].lower(),
                'variable': splitLine[6][int(splitLine[5])],
                'coefficient': splitLine[7][-1]
            }
            data = []

            continue

        
        data.append(splitLine[-4:])
